//
//  VersionString.h
//  food
//
//  Created by 詹 迟晶 on 12-5-7.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VersionString : NSObject
+ (BOOL)isMatchVersion:(NSString *)current withExpression:(NSString *)exp;
+ (BOOL)isMatchVersionWithExpression:(NSString *)exp;
@end
