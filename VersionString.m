//
//  VersionString.m
//  food
//
//  Created by Whirlwind James on 12-5-7.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "VersionString.h"

@implementation VersionString
/** 判断版本是否和版本表达式匹配
 *
 *  current可以与">1.1"、"<1.1"、">=1.1"、"<=1.1"、"1.1"、"=1.1"、"~=1.1"等比较
 *  
 *      ">1.1"  大于1.1版本，1.1.1返回YES，1.1返回NO
 *      "<1.1"  小于1.1版本，1.1返回NO，1.0.1返回YES
 *      ">=1.1"与"<=1.1" 与上面两个相似
 *      "1.1"等价于"=1.1"
 *      "~=1.1" 忽略子版本，前面版本号相同即返回YES
 *      "<>1.1"与"~=1.1" 不等于1.1，注意，会忽略子版本号，即1.1.1返回NO，1.2返回YES
 *
 *  支持比较符：
 *      >、<、>=、<=、~=、<>、!=
 *  @param  current
 *
 *  @return 匹配结果，boolean类型
 */
+ (BOOL)isMatchVersion:(NSString *)current withExpression:(NSString *)exp{
    if (current == nil) {
        return NO;
    }
    NSError *error = NULL;
    exp = [exp stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"([<~>=]*)(\\d+[.\\d]*)"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    
    NSString *s1 = [regex stringByReplacingMatchesInString:exp
                                                   options:0
                                                     range:NSMakeRange(0, [exp length])
                                              withTemplate:@"$1"];
    NSString *s2 = [regex stringByReplacingMatchesInString:exp
                                                   options:0
                                                     range:NSMakeRange(0, [exp length])
                                              withTemplate:@"$2"];
    ;
    
    BOOL state = YES;
    NSMutableArray *currentVersionArray = [[NSMutableArray alloc] initWithArray:[current componentsSeparatedByString:@"."]];
    NSMutableArray *expressVersionArray = [[NSMutableArray alloc] initWithArray:[s2 componentsSeparatedByString:@"."]];
    for (int i = [currentVersionArray count] - 1; i < [expressVersionArray count] - 1; i++) {
        [currentVersionArray addObject:@"0"];
    }
    if ([s1 isEqualToString:@"~="]) {
        for (int i = 0; i < [expressVersionArray count]; i++) {
            int cver = [(NSString *)[currentVersionArray objectAtIndex:i] intValue];
            int ever = [(NSString *)[expressVersionArray objectAtIndex:i] intValue];
            if (cver != ever) {
                state = NO;
                break;
            }
        }
    }else if ([s1 isEqualToString:@"<>"] || [s1 isEqualToString:@"!="]) {
        for (int i = 0; i < [expressVersionArray count]; i++) {
            int cver = [(NSString *)[currentVersionArray objectAtIndex:i] intValue];
            int ever = [(NSString *)[expressVersionArray objectAtIndex:i] intValue];
            if (cver == ever) {
                state = NO;
                break;
            }
        }
    }else {
        for (int i = [expressVersionArray count] - 1; i < [currentVersionArray count] - 1; i++) {
            [expressVersionArray addObject:@"0"];
        }
        BOOL isEqual = NO;
        for (int i = 0; i < [currentVersionArray count]; i++) {
            int cver = [(NSString *)[currentVersionArray objectAtIndex:i] intValue];
            int ever = [(NSString *)[expressVersionArray objectAtIndex:i] intValue];
            if ([s1 isEqualToString:@">"]) {
                if (cver < ever){
                    state = NO;
                    isEqual = NO;
                    break;
                }else if (cver > ever) {
                    state = YES;
                    isEqual = NO;
                    break;
                }else if (cver == ever) {
                    isEqual = YES;
                }
            }else if ([s1 isEqualToString:@">="]) {
                if (cver < ever){
                    state = NO;
                    isEqual = NO;
                    break;
                }else if (cver > ever) {
                    state = YES;
                    isEqual = NO;
                    break;
                }else if (cver == ever) {
                    isEqual = YES;
                }
            }else if ([s1 isEqualToString:@"<"]) {
                if (cver > ever){
                    state = NO;
                    isEqual = NO;
                    break;
                }else if (cver < ever) {
                    state = YES;
                    isEqual = NO;
                    break;
                }else if (cver == ever) {
                    isEqual = YES;
                }
            }else if ([s1 isEqualToString:@"<="]) {
                if (cver > ever){
                    state = NO;
                    isEqual = NO;
                    break;
                }else if (cver < ever) {
                    state = YES;
                    isEqual = NO;
                    break;
                }else if (cver == ever) {
                    isEqual = YES;
                }
            }else if ([s1 isEqualToString:@"="] || [s1 isEqualToString:@""]) {
                if (cver == ever) {
                    state = YES;
                    isEqual = NO;
                }else {
                    state = NO;
                    break;
                }
            }
        }
        if (state && isEqual) {
            if ([s1 isEqualToString:@">"] || [s1 isEqualToString:@"<"]) {
                state = NO;
            }
        }
    }
    [currentVersionArray release];
    [expressVersionArray release];
    return state;
}

+ (BOOL)isMatchVersionWithExpression:(NSString *)exp{
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"];
    return [self isMatchVersion:appVersion withExpression:exp];
}
@end
